package ru.mikaev.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.mikaev.dto.DivisionDto;
import ru.mikaev.entities.Division;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface DivisionMapper {
    DivisionDto fromDivision(Division division);
    Division toDivision(DivisionDto dto);
}
