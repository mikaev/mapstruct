package ru.mikaev.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import ru.mikaev.dto.DivisionDto;
import ru.mikaev.dto.EmployeeDto;
import ru.mikaev.entities.Division;
import ru.mikaev.entities.Employee;

@Mapper(componentModel = "spring", uses = DivisionMapper.class)
public interface EmployeeMapper {
    @Mappings ({
            @Mapping(source = "firstName", target = "fname"),
            @Mapping(source = "lastName", target = "lname"),
            @Mapping(source = "division", target = "divisionDto")
    })
    EmployeeDto fromEmployee(Employee employee);

    @Mappings ({
            @Mapping(source = "fname", target = "firstName"),
            @Mapping(source = "lname", target = "lastName"),
            @Mapping(source = "divisionDto", target = "division")
    })
    Employee toEmployee(EmployeeDto employeeDto);
}
