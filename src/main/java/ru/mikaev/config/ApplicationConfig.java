package ru.mikaev.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.mikaev.entities.Division;

@Configuration
public class ApplicationConfig {
    @Bean
    Division division(){
        return Division.builder().id(1).name("division").build();
    }
}
