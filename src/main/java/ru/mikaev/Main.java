package ru.mikaev;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.mikaev.dto.DivisionDto;
import ru.mikaev.dto.EmployeeDto;
import ru.mikaev.entities.Employee;
import ru.mikaev.mappers.DivisionMapper;
import ru.mikaev.mappers.EmployeeMapper;
import ru.mikaev.entities.Division;

@SpringBootApplication
public class Main implements CommandLineRunner {
    @Autowired
    private EmployeeMapper converter;

    @Autowired
    private DivisionMapper divisionMapper;

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }

    @Override
    public void run(String... args) throws Exception {
        EmployeeDto simpleDto = converter.fromEmployee(new Employee("Nikita", "Mikaev", new Division(1, "Artem", applicationContext.getBean(Division.class))));
        Employee employee = converter.toEmployee(simpleDto);
        System.out.println(simpleDto);
        System.out.println(employee);
    }
}
