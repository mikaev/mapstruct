package ru.mikaev.mappers;

import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import ru.mikaev.dto.DivisionDto;
import ru.mikaev.entities.Division;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class DivisionMapperImpl implements DivisionMapper {

    @Override
    public DivisionDto fromDivision(Division division) {
        if ( division == null ) {
            return null;
        }

        DivisionDto divisionDto = new DivisionDto();

        divisionDto.setName( division.getName() );

        return divisionDto;
    }

    @Override
    public Division toDivision(DivisionDto dto) {
        if ( dto == null ) {
            return null;
        }

        Division division = new Division();

        division.setName( dto.getName() );

        return division;
    }
}
