package ru.mikaev.mappers;

import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mikaev.dto.EmployeeDto;
import ru.mikaev.entities.Employee;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class EmployeeMapperImpl implements EmployeeMapper {

    @Autowired
    private DivisionMapper divisionMapper;

    @Override
    public EmployeeDto fromEmployee(Employee employee) {
        if ( employee == null ) {
            return null;
        }

        EmployeeDto employeeDto = new EmployeeDto();

        employeeDto.setFname( employee.getFirstName() );
        employeeDto.setLname( employee.getLastName() );
        employeeDto.setDivisionDto( divisionMapper.fromDivision( employee.getDivision() ) );

        return employeeDto;
    }

    @Override
    public Employee toEmployee(EmployeeDto employeeDto) {
        if ( employeeDto == null ) {
            return null;
        }

        Employee employee = new Employee();

        employee.setDivision( divisionMapper.toDivision( employeeDto.getDivisionDto() ) );
        employee.setFirstName( employeeDto.getFname() );
        employee.setLastName( employeeDto.getLname() );

        return employee;
    }
}
